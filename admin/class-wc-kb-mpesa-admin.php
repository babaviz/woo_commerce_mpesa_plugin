<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/admin
 * @author     FutureVH <https://futurevh.com>
 */
class WC_Kb_Mpesa_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wc_kb_mpesa    The ID of this plugin.
	 */
	private $wc_kb_mpesa;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;


	/**
	 * The setting options for this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $options    Options for this plugin.
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wc_kb_mpesa       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wc_kb_mpesa, $version ) {

		$this->wc_kb_mpesa = $wc_kb_mpesa;
		$this->version = $version;

	}

	/**
	 * Initialize Gateway Settings Form Fields
	 */
	public function init_wc_gateway() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wc-kb-mpesa-gateway.php';
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->wc_kb_mpesa, plugin_dir_url( __FILE__ ) . 'css/wc-kb-mpesa-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in WC_Kb_Mpesa_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The WC_Kb_Mpesa_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wc_kb_mpesa, plugin_dir_url( __FILE__ ) . 'js/wc-kb-mpesa-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function add_wc_kb_mpesa_gateway( $methods ) {
		$methods[] = 'WC_Kb_Mpesa_Gateway'; 
		return $methods;
	}

	public function mpesa_payments_post_type(){
		register_post_type(
			'wc_kb_mpesa_payment',
			[
				'labels' => [
					'name'          => __('MPESA Payments'),
					'singular_name' => __('MPESA Payment'),
				],
				'supports' => [ 
					'title',
					'comments'
				],
				'public'      => true,
				'has_archive' => true,
				'publicly_queryable' => false
			]
		);
	}

	public function mpesa_checkouts_post_type(){
		register_post_type(
			'wc_kb_mpesa_checkout',
			[
				'labels' => [
					'name'          => __('MPESA Checkouts'),
					'singular_name' => __('MPESA Checkout'),
				],
				'supports' => [ 
					'title', 
					'comments'
				],
				'public'      => true,
				'has_archive' => true,
				'publicly_queryable' => false,
				'show_in_menu' => false,
				'show_ui' => false
			]
		);
	}

	public function mpesa_checkouts_custom_fields(){
		add_meta_box("amount", "Amount", "amount_cb", "wc_kb_mpesa_checkout", "normal", "high");
		add_meta_box("request_id", "Request Id", "request_id_cb", "wc_kb_mpesa_checkout", "normal", "high");
		add_meta_box("order_id", "Order Id", "order_id_cb", "wc_kb_mpesa_checkout", "normal", "high");
		
		function amount_cb(){
			global $post;
			$custom = get_post_custom($post->ID);
			//error_log(json_encode($post));
			//error_log(json_encode($custom));
			$amount = $custom && !empty($custom["amount"]) ? $custom["amount"][0] : '';
			?>
			<label>Amount:</label>
			<input name="amount" value="<?php echo $amount; ?>" />
			<?php
		}

		function request_id_cb(){
			global $post;
			$custom = get_post_custom($post->ID);
			$request_id = $custom && !empty($custom["request_id"]) ? $custom["request_id"][0] : '';
			?>
			<label>Request ID:</label>
			<input name="request_id" value="<?php echo $request_id; ?>" />
			<?php
		}

		function order_id_cb(){
			global $post;
			$custom = get_post_custom($post->ID);
			$order_id = $custom && !empty($custom["order_id"]) ? $custom["order_id"][0] : '';
			?>
			<label>Order Id:</label>
			<input name="order_id" value="<?php echo $order_id; ?>" />
			<?php
		}
	}

	function update_checkouts_custom_fields(){
		global $post;
		!empty($_POST["amount"]) ? update_post_meta($post->ID, "amount", $_POST["amount"]) : '';
		!empty($_POST["request_id"]) ? update_post_meta($post->ID, "request_id", $_POST["request_id"]): '';
		// !empty($_POST["order_id"]) ? update_post_meta($post->ID, "order_id", $_POST["order_id"]) : '';
	}

	public function mpesa_payments_custom_fields(){
		add_meta_box("customer_name", "Customer Name", "customer_name_cb", "wc_kb_mpesa_payment", "normal", "high");
		add_meta_box("phone", "Phone Number", "payment_phone_cb", "wc_kb_mpesa_payment", "normal", "high");
		add_meta_box("transaction_code", "Transaction Code", "transaction_code_cb", "wc_kb_mpesa_payment", "normal", "high");
		add_meta_box("order_id", "Order", "payment_order_id_cb", "wc_kb_mpesa_payment", "normal", "high");
		
		function customer_name_cb(){
			global $post;
			$custom = get_post_custom($post->ID);
			$customer_name = $custom && !empty($custom["customer_name"]) ? $custom["customer_name"][0] : '';
			?>
			<span name="customer_name"><?php echo $customer_name; ?></span>
			<?php
		}

		function payment_phone_cb(){
			global $post;
			$custom = get_post_custom($post->ID);
			$phone = $custom && !empty($custom["phone"]) ? $custom["phone"][0] : '';
			?>
			<span name="amount"><?php echo $phone; ?></span>
			<?php
		}

		function transaction_code_cb(){
			global $post;
			$custom = get_post_custom($post->ID);
			$transaction_code = $custom && !empty($custom["transaction_code"]) ? $custom["transaction_code"][0] : '';
			?>
			<span name="transaction_code"><?php echo $transaction_code; ?></span>
			<?php
		}

		function payment_order_id_cb(){
			global $post;
			$custom = get_post_custom($post->ID);
			$order_id = $custom && !empty($custom["order_id"]) ? $custom["order_id"][0] : '';
			$url = '';
			if(!empty($order_id)){
				$order = new WC_Order($order_id);
				$url = "<a href=".$order->get_view_order_url()." target='_blank'>View Order</a>";
			}
			?>
			<span><?php echo $url; ?></span>
			<?php
		}
	}
}
