<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    WC_Kb_Mpesa
 * @subpackage WC_Kb_Mpesa/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    WC_Kb_Mpesa
 * @subpackage WC_Kb_Mpesa/includes
 * @author     FutureVH <https://futurevh.com>
 */

require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/mpesaApi/autoload.php';

use Kabangi\Mpesa\Native\Mpesa;

class WC_Kb_Mpesa {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WC_Kb_Mpesa_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $wc_kb_mpesa    The string used to uniquely identify this plugin.
	 */
	protected $wc_kb_mpesa;

	/**
	 * The mpesa client that is used to make calls to the api.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Mpesa    $mpesa_client    The mpesa object that is used to make calls to mpesa platform.
	 */
	protected $mpesa_client;

	/**
	 * The mpesa client settings that is used to make calls to the api.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Array    $mpesa_client    The mpesa array settings that is used to make calls to mpesa platform.
	 */
	protected $mpesa_api_settings;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WC_KB_MPESA_VERSION' ) ) {
			$this->version = WC_KB_MPESA_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->wc_kb_mpesa = 'mpesa';

		$this->mpesa_client = new Mpesa($this->get_mpesa_api_settings());
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->add_settings_link();

	}

	public function add_settings_link(){
		
	}

	public function plugin_settings_link(){
		$url = get_admin_url() . 'admin.php?page=wc-settings&tab=checkout&section=wc_kb_mpesa';
		$settings_link = '<a href="'.$url.'">' . __( 'Settings', 'textdomain' ) . '</a>';
		array_unshift( $links, $settings_link );
		return $links;
	}

	private function get_mpesa_api_settings(){
		// Pull plugin options
		// ANTI-PATTERN But works
		$options = get_option('woocommerce_wc_kb_mpesa_settings');
		$lmno_cred = [];
		if(count($options) > 0){
			foreach($options as $key => $val){
				if(strpos($key,'lnmo') > -1){
					$keyTokens = explode('_',$key);
					array_shift($keyTokens);
					$key = implode('_',$keyTokens);
					$lmno_cred[$key] = $val;
				}
			}
		
			$this->mpesa_api_settings = [
				'consumer_key' => !empty($options['consumer_key']) ? $options['consumer_key'] : '',
				'consumer_secret' => !empty($options['consumer_secret']) ? $options['consumer_secret'] : '',
				'lnmo' => $lmno_cred,
				'is_sandbox' => !empty($options['is_sandbox']) && $options['is_sandbox'] == 'yes' ? true : false,
			];
		}
		//error_log(json_encode($this->mpesa_api_settings));
		return $this->mpesa_api_settings;
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - WC_WC_Kb_Mpesa_Loader. Orchestrates the hooks of the plugin.
	 * - WC_WC_Kb_Mpesa_i18n. Defines internationalization functionality.
	 * - WC_WC_Kb_Mpesa_Admin. Defines all hooks for the admin area.
	 * - WC_WC_Kb_Mpesa_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wc-kb-mpesa-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-wc-kb-mpesa-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-wc-kb-mpesa-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-wc-kb-mpesa-public.php';
		$this->loader = new WC_Kb_Mpesa_Loader();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the WC_WC_Kb_Mpesa_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new WC_Kb_Mpesa_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new WC_Kb_Mpesa_Admin( $this->get_wc_kb_mpesa(), $this->get_version() );
		
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'woocommerce_api_wc_kb_mpesa_gateway',$this, 'handle_mpesa_ipn_response');
		$this->loader->add_action( 'plugins_loaded', $plugin_admin, 'init_wc_gateway' );
		$this->loader->add_filter( 'woocommerce_payment_gateways', $plugin_admin, 'add_wc_kb_mpesa_gateway' );
		$this->loader->add_action('init',$plugin_admin, 'mpesa_payments_post_type');
		$this->loader->add_action('init',$plugin_admin, 'mpesa_checkouts_post_type');
		$this->loader->add_action("admin_init", $plugin_admin, "mpesa_checkouts_custom_fields");
		$this->loader->add_action("save_post", $plugin_admin, "update_checkouts_custom_fields");
		$this->loader->add_action("admin_init", $plugin_admin, "mpesa_payments_custom_fields");
		// $this->loader->add_action("save_post", $plugin_admin, "update_payments_custom_fields");
		$this->loader->add_action('wp_ajax_nopriv_wc_kb_mpesa_checkout',$this, 'handle_mpesa_checkout');
		$this->loader->add_action('wp_ajax_nopriv_wc_kb_mpesa_checkout_status',$this, 'check_stk_push_status');
		$this->loader->add_action('wp_ajax_nopriv_wc_kb_order_checkout_exists',$this, 'order_checkout_exists');
		
	}
	
    public function register_for_C2B(){
		$confirmation_url = get_site_url().'?source=mpesa&purpose=payment';
		$validation_url = get_site_url().'?source=mpesa&purpose=validation';
		try{
			$response = $this->mpesa_client->C2BRegister([
				'shortCode'       => $this->mpesa_api_settings['lnmo']['short_code'],
				'confirmationURL' => $confirmation_url,
				'validationURL'   => $validation_url
			]);
			// TODO: Display a wordpress admin notice if it fails.
			//error_log(json_encode($response));
		}catch(Exception $e){

		}
	}
  
	private function c2b_confirmation(){
		$paymentMeta = [
		  "Amount" => $_POST['TransAmount'],
		  "Time" => $_POST['TransTime'],
		  "PhoneNumber" => $_POST['MSISDN'],
		  "MpesaReceiptNumber" => $_POST['TransID']
		];
		$checkout = $this->get_checkout_by_request_id($_POST['BillRefNumber']);
		$checkout = $this->add_metas_to_post_object($checkout->ID);
		$this->record_payment($paymentMeta,$checkout);
	}
  
	public function c2b_validation(){
		return $_POST;
	}

	public function handle_c2b_payments(){
		$is_our_callback = !empty($_GET['source']) && !empty($_GET['purpose']) 
		&& $_GET['source'] == 'mpesa';

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' && $is_our_callback == true) {
			$purpose = $_GET['purpose'];
			switch($purpose){
				case 'validation':
					$this->c2b_validation();
					break;
				case 'payment':
					$this->c2b_confirmation();
					break;
				default:
					// Do nothing
			}
		}
	}

	private function define_public_hooks(){
		$plugin_public = new WC_Kb_Mpesa_Public( $this->get_wc_kb_mpesa(), $this->get_version() );
		$this->loader->add_action('wp_ajax_wc_kb_mpesa_checkout',$this, 'handle_mpesa_checkout');
		$this->loader->add_action('wp_ajax_wc_kb_mpesa_checkout_status',$this, 'check_stk_push_status');
		$this->loader->add_action('wp_ajax_wc_kb_order_checkout_exists',$this, 'order_checkout_exists');
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		
		// Register C2B payments and it's handler
		$this->loader->add_action('wp_loaded',$this, 'handle_c2b_payments');
		$this->loader->add_action('woocommerce_settings_saved',$this, 'register_for_C2B');
	}

	public function handle_mpesa_ipn_response(){
		$posted = json_decode(file_get_contents('php://input'),true);
		if ( ! empty( $posted ) && $this->validate_ipn() ) {
			error_log('ipn received!');
			$this->mpesa_checkout_completed($posted);
			exit;
		}
		//error_log('Mpesa IPN Request Failure');
		wp_die( 'Mpesa IPN Request Failure', 'Mpesa IPN', array( 'response' => 500 ) );
	}

	public function record_payment($payment,$checkout_post){
		global $woocommerce;
		$checkout_post = $this->add_metas_to_post_object($checkout_post->ID);
		// Record payment on the database.
		$order = new WC_Order($checkout_post->order_id);
		$post_id = wp_insert_post([
			'post_title' => $order->get_formatted_billing_full_name(),
			'post_status' => 'publish',
			'post_type' => 'wc_kb_mpesa_payment',
		]);
		if ($post_id) {
			// insert post meta
			$metas = [
				'amount' => $payment["Amount"],
				'customer_name' => $order->get_formatted_billing_full_name(),
				'checkout_id' => $checkout_post->ID,
				'order_id' => $checkout_post->order_id,
				'phone' => $payment["PhoneNumber"],
				'transaction_code' => $payment["MpesaReceiptNumber"]
			];
			$this->update_post(
				$post_id,
				$metas
			);
		}
		// Mark order as being paid for.
		$order->payment_complete();
		// Remove cart.
		WC()->cart->empty_cart();
		return array(
			'result'   => 'success',
			'redirect' => '',
		);
	}

	/**
	 * Update any post_type given to this function
	 * @since    1.0.0
	 * @param integer $post_id  Wordpress grobal post id
	 * @param array   $metas 	Associative array of the post meta params to set for the post
	 * @param array   $post_details Associative array of ordinary parameters supported by wordpress 
	 * 								that you may want to update.
	 */
	protected function update_post($post_id,$metas = [] , $post_details = []){
		foreach($metas as $key => $val){
			update_post_meta($post_id, $key, $val);
		}
		// TODO: work on updating the details such as author, title etc.
	}

	public function mpesa_checkout_completed($posted){
		// TODO: Log this request.
        //error_log(json_encode($posted));
        $data = $posted['Body'];
		$data = $data['stkCallback'];
		$resultCode = $data['ResultCode'];
		// Pull the active checkout
		$checkout = $this->get_checkout_by_request_id($data['CheckoutRequestID']);
		$checkout = $this->add_metas_to_post_object($checkout->ID);
		if(isset($data['ResultCode']) && !empty($checkout->ID)){
			switch($resultCode){
				case 0:
					$paymentMeta = [
					'CheckoutRequestID' => $data['CheckoutRequestID']
					];
					foreach($data['CallbackMetadata']['Item'] as $k => $v){
						$v = (object) $v;
						if(!empty($v->Name) && !empty($v->Value)){
							$paymentMeta[$v->Name] = $v->Value;
						}
					}
					//error_log(json_encode($paymentMeta));
					//error_log(json_encode($data['CallbackMetadata']));
					$this->record_payment($paymentMeta,$checkout);
					$this->update_post($checkout->ID,[
						'description' => 'Payment received successfully.',
						'status' => 'PROCESSED'
					]);
					break;
				case 1001:
					$this->update_post($checkout->ID,[
						'description' => 'You have an active payment session. Please wait please trying again',
						'description_type' => 'warning',
						'status' => 'FAULTY'
					]);
					break;
				case 1037: 
					// Timed out before a payment was received
					$this->update_post($checkout->ID,[
						'description' => $this->get_manual_pay_message($checkout->order_id,$checkout->amount),
						'description_type' => 'info',
						'status' => 'TIMEDOUT'
					]);
					break;
				case 1036:
					$this->update_post($checkout->ID,[
						'description' => $this->get_manual_pay_message($checkout->order_id,$checkout->amount),
						'description_type' => 'info',
						'status' => 'NOTSUPPORTED'
					]);
					break;
				case 1032:
					$this->update_post($checkout->ID,[
						'description' => 'You have cancelled the payment request. Please try again.',
						'description_type' => 'warning',
						'status' => 'CANCELLED'
					]);
				default:
					$this->update_post($checkout->ID,[
						'description' => $this->get_manual_pay_message($checkout->order_id,$checkout->amount),
						'description_type' => 'info',
						'status' => 'CANCELLED'
					]);
			}
		}
	}

	private function get_unprocessed_checkout_by_order_id($order_id){
		//error_log($order_id);
		$args= [
			'post_type' => 'wc_kb_mpesa_checkout',
			'orderby' 	=> 'menu_order',
			'order' 	=> 'ASC',
			'meta_query' => [
				[
					'key'     => 'order_id',
					'value'   => $order_id,
					'compare' => '=',
				],
				[
					'key'     => 'status',
					'value'   => 'INPROGRESS',
					'compare' => '=',
				]
			]
		];

		$the_query = new WP_Query( $args );
		$post = null;
		if($the_query->have_posts()){
			$posts = $the_query->get_posts();
			if(count($posts) == 1){
				$post = $posts[0];
			}
		}
		if(empty($post)){
			return new WP_Error( '404', __("Checkout with that criteria does not exist.", "wc_kb_mpesa" ) );
		}
		return $post;
	}

	private function get_checkout_by_request_id($request_id){
		$args= [
			'post_type' => 'wc_kb_mpesa_checkout',
			'orderby' 	=> 'menu_order',
			'order' 	=> 'ASC',
			'meta_query' => [
				[
					'key'     => 'request_id',
					'value'   => $request_id,
					'compare' => '=',
				]
			]
		];

		$the_query = new WP_Query( $args );
		$post = null;
		if($the_query->have_posts()){
			$posts = $the_query->get_posts();
			if(count($posts) == 1){
				$post = $posts[0];
			}
		}
		return $post;
	}

	public function handle_mpesa_checkout(){
		$isAllowed = check_ajax_referer( 'wc_kb_mpesa_token',$_POST,false);
		if($isAllowed === false){
			wp_send_json_error([
				"message" => "You are not allowed here. Confirm you are using the right plugin for the job"
			],403); 
		}
		$response = $this->process_checkout($_POST);
		if( is_wp_error( $response ) ) {
			wp_send_json_error([
				"message" => $response->get_error_message()
			],$response->get_error_code()); 
		}else{
			wp_send_json ($response,200);
		} 
	}

	public function order_checkout_exists(){
		$isAllowed = check_ajax_referer( 'wc_kb_mpesa_token',$_POST,false);
		if($isAllowed === false){
			wp_send_json_error([
				"message" => "You are not allowed here. Confirm you are using the right plugin for the job"
			],403); 
		}

		$response = $this->get_unprocessed_checkout_by_order_id($_POST['order_id']);
		if( is_wp_error( $response ) ) {
			wp_send_json_error([
				"message" => $response->get_error_message()
			],$response->get_error_code()); 
		}else{
			$checkout = $this->add_metas_to_post_object($response->ID);
			wp_send_json ($checkout,200);
		}
		
	}

	public function check_stk_push_status(){
		$isAllowed = check_ajax_referer( 'wc_kb_mpesa_token',$_POST,false);
		if($isAllowed === false){
			wp_send_json_error([
				"message" => "You are not allowed here. Confirm you are using the right plugin for the job"
			],403); 
		}
		
		$response = null;
		try{
			// Get the existing checkout request.
			$checkout = $this->add_metas_to_post_object($_POST['ID']);
            if($checkout->status !== 'INPROGRESS'){
                $response = $checkout;
            }
            $mpRes = $this->mpesa_client->STKStatus([
                'checkoutRequestID' => $checkout->request_id
			]);
			//error_log(json_encode($mpRes));
            if(isset($mpRes->ResultCode)){
                // Update the local persisted checkouts ResultCode
                $resultCode = intval($mpRes->ResultCode);
                switch($resultCode){
                    case 0:
						$this->update_post($checkout->ID,[
							'description' => 'Payment received successfully.',
							'status' => 'PROCESSED'
						]);
                        break;
                    case 1001:
						$this->update_post($checkout->ID,[
							'description' => 'You have an active payment session. Please wait please trying again',
							'description_type' => 'warning',
							'status' => 'FAULTY'
						]);
                        break;
                    case 1037: 
                        // Timed out before a payment was received
						$this->update_post($checkout->ID,[
							'description' => $this->get_manual_pay_message($checkout->order_id,$checkout->amount),
							'description_type' => 'info',
							'status' => 'TIMEDOUT'
						]);
                        break;
                    case 1036:
						$this->update_post($checkout->ID,[
							'description' => $this->get_manual_pay_message($checkout->order_id,$checkout->amount),
							'description_type' => 'info',
							'status' => 'NOTSUPPORTED'
						]);
                        break;
                    case 1032:
						$this->update_post($checkout->ID,[
							'description' => 'You have cancelled the payment request. Please try again.',
							'description_type' => 'warning',
							'status' => 'CANCELLED'
						]);
                    default:
						$this->update_post($checkout->ID,[
							'description' => $this->get_manual_pay_message($checkout->order_id,$checkout->amount),
							'description_type' => 'info',
							'status' => 'CANCELLED'
						]);

				}
				$response = $this->add_metas_to_post_object($checkout->ID);
            }else{
                $response = $checkout;
            }

        }catch(\Exception $e){
            $response = new WP_Error( '503', __($e->getMessage(), "wc_kb_mpesa" ) );
		}
		
		if( is_wp_error( $response ) ) {
			wp_send_json_error([
				"message" => $response->get_error_message()
			],$response->get_error_code()); 
		}else{
			wp_send_json ($response,200);
		}
	}

	public function process_checkout($data){
		// Prevent double payment by always returning any pending INPROGRESS payment for this order.
		$existingCheckout = $this->get_unprocessed_checkout_by_order_id($data['order_id']);
		if(!is_wp_error($existingCheckout)){
			return $this->add_metas_to_post_object($existingCheckout->ID);
		}

		// Continue with checkout if nothing exists.

        $order = new WC_Order( $data['order_id']);
        $amount = $order->get_total();
        $phone = $order->get_billing_phone();
        $first_name = $order->get_billing_first_name();
        $last_name = $order->get_billing_last_name();
		$phone = preg_replace('/^(0|\+254)/', '254', $phone);

		// Create a checkout after response.
		$callbackUrl = WC()->api_request_url( 'WC_Kb_Mpesa_Gateway' );

		// Return that.
		$mpesaParams = [
			'amount' => intval($amount),
			'phoneNumber' => $phone,
			'accountReference' => $data['order_id'],
			'callBackURL' => $callbackUrl,
			'transactionDesc' => "Checkout",
		];
		//error_log(json_encode($mpesaParams));
		$response = $this->mpesa_client->STKPush($mpesaParams);
		//error_log(json_encode($response));
		if(!empty($response->CheckoutRequestID)){
			// Create a check out post.
			$post_id = wp_insert_post([
				'post_title' => $order->get_formatted_billing_full_name(),
				'post_status' => 'publish',
				'post_type' => 'wc_kb_mpesa_checkout',
			]);

			if ($post_id) {
				$this->update_post($post_id,[
					'amount' => $amount,
					'status' => 'INPROGRESS',
					'description' => 'Waiting for payment from your mpesa account.',
					'request_id' => $response->CheckoutRequestID,
					'order_id' =>  $data['order_id'],
				]);
			}
			return $this->add_metas_to_post_object($post_id);
        }else{
			return new WP_Error( '503', __( "Something went wrong. Please try again", "wc_kb_mpesa" ) );
		}
	}

	private function get_manual_pay_message($order_no,$amount){
		$paybill = $this->mpesa_api_settings['lnmo']['short_code'];

		$msg = "We are having an issue sending payment request to your phone. Please follow the instructions below to make a payment.<br/>".
		"1. Go to Safaricom SIM Tool Kit, select M-PESA menu, select \"Lipa na M-PESA\".<br/>".
		"2. Select \"Pay Bill\".<br/>".
		"3. Enter Paybill No.{$paybill}.<br/>".
		"4. Enter account no as {$order_no}.<br/>".
		"5. Enter amount as {$amount}.<br/>".
		"6. Enter your M-PESA PIN and press \"OK\".<br/>";

		if($this->mpesa_api_settings['lnmo']['merchant_type']=='till'){
            $msg = "We are having an issue sending payment request to your phone. Please follow the instructions below to make a payment.<br/>".
                "1. Go to Safaricom SIM Tool Kit, select M-PESA menu, select \"Lipa na M-PESA\".<br/>".
                "2. Select \"Buy Goods and Services\".<br/>".
                "3. Enter Till No.{$paybill}.<br/>".
                "4. Enter amount as {$amount}.<br/>".
                "5. Enter your M-PESA PIN and press \"OK\".<br/>";
        }

		return $msg;
	}

	public function add_metas_to_post_object($post_id){
		$post = get_post($post_id);
		if($post){
			$metas = get_post_meta($post_id);
			foreach($metas as $key => $val){
				$post->{$key} = is_array($val) ? $val[0] : $val;
			}
		}
		return $post;
	}
	
	/**
	 * Check IPN validity.
	 * Basically confirm this checkout exists on safaricom's infra before proceeding
	 * 
	 */
	public function validate_ipn() {
		return true;
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_wc_kb_mpesa() {
		return $this->wc_kb_mpesa;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    WC_WC_Kb_Mpesa_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
