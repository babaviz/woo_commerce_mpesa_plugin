<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/admin
 * @author     FutureVH <https://futurevh.com>
 */
require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/mpesaApi/autoload.php';

use Kabangi\Mpesa\Native\Mpesa;

class WC_Kb_Mpesa_Gateway extends WC_Payment_Gateway {
	/** @var bool Whether or not logging is enabled */
	public static $log_enabled = false;
	
	/** @var WC_Logger Logger instance */
	public static $log = false;

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wc_kb_mpesa    The ID of this plugin.
	 */
	private $wc_kb_mpesa;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;


	/**
	 * The setting options for this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $options    Options for this plugin.
	 */
	private $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wc_kb_mpesa       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct() {
		$this->id = 'wc_kb_mpesa';
		$this->method_title = 'MPESA';
		$this->method_description = "MPESA Payment Gateway";
		$this->has_fields = false;
        $this->init_form_fields();
        $this->init_settings();
        
        // Define user set variables
        $this->title        = $this->get_option( 'title' );
        $this->description  = $this->get_option( 'description' );
        $this->instructions = $this->get_option( 'instructions', $this->description );
    
        // Actions
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
		add_action( 'woocommerce_receipt_' . $this->id, [$this, 'receipt_page']);
	}

	/**
	 * Initialize Gateway Settings Form Fields
	 */
	public function init_form_fields() {
		
		$this->form_fields = apply_filters( 'wc_kb_mpesa_form_fields', [
			'api_details' => [
				'title'       => __( 'API Credentials', 'wc-kb-mpesa' ),
				'type'        => 'title',
                'description' => sprintf( __( 'Enter your MPESA Lipa Na Mpesa Online API credentials to allow processing of MPESA payments from your users. Learn how to access your <a href="%s" target="_blank">Lipa na mpesa API Credentials</a>.', 'wc-kb-mpesa' ), 'https://developer.safaricom.co.ke/user/me/apps' ),
            ],
			'consumer_key' => [
				'title'       => __( 'Consumer Key', 'wc-kb-mpesa' ),
				'type'        => 'text',
				'description' => __( 'Get your API credentials from Safaricom.', 'wc-kb-mpesa' ),
				'default'     => '',
				'desc_tip'    => true,
				'placeholder' => __( 'Consumer Key from Safaricom', 'wc-kb-mpesa' ),
            ],
			'consumer_secret' => [
				'title'       => __( 'Consumer Secret', 'wc-kb-mpesa' ),
				'type'        => 'text',
				'description' => __( 'Get your API credentials from Safaricom.', 'wc-kb-mpesa' ),
				'default'     => '',
				'desc_tip'    => true,
				'placeholder' => __( 'Consumer Secret from Safaricom', 'wc-kb-mpesa' ),
            ],

            'lnmo_passkey' => [
                'title'       => __( 'Pass Key', 'wc-kb-mpesa-passkey' ),
                'type'        => 'text',
                'description' => __( 'Pass key to initiate the payment via the API. This is issued by safaricom after you have submitted for GO Live approval.', 'wc-kb-mpesa' ),
                'default'     => __( '', 'wc-kb-mpesa' ),
                'desc_tip'    => true,
            ],

			'is_sandbox' => [
				'title'       => __( 'Enable MPESA sandbox', 'wc-kb-mpesa' ),
				'type'        => 'checkbox',
				'description' => sprintf( __( 'MPESA sandbox enables you to test payments. <a href="%s" target="_blank">Sign up for a developer account..</a>.', 'wc-kb-mpesa' ), 'https://developer.safaricom.co.ke/user/me/apps' ),
				'default'     => 'no',
				'placeholder' => __( 'Enable payments in sandbox environment', 'wc-kb-mpesa' ),
            ],


			'lnmo_hr1' => [
				'title'       => __( ' ', 'wc-kb-mpesa' ),
				'type'        => 'title',
				'description' => sprintf( __( '<hr>', 'wc-kb-mpesa' )),
            ],

			'lnmo_merchant_desc' => [
				'title'       => __( 'MPESA MERCHANT', 'wc-kb-mpesa' ),
				'type'        => 'title',
				'description' => sprintf( __( '<hr>', 'wc-kb-mpesa' )),
            ],

            'enabled' => [
                'title'   => __( 'Enable/Disable', 'wc-kb-mpesa' ),
                'type'    => 'checkbox',
                'label'   => __( 'Enable Mpesa Payment', 'wc-kb-mpesa' ),
                'default' => 'yes'
            ],

			'lnmo_merchant_type'=>[
			    'title'=>__('Payment Type','wc-kb-mpesa'),
                'type'=>'select',
                'options'=>['paybill'=>'PAYBILL NUMBER','till'=>'TILL NUMBER'],
                'description'=>__('Are you using a paybill or a till number?','wc-kb-mpesa')
            ],

            'lnmo_short_code' => [
                'title'       => __( 'Paybill/Till Number', 'wc-kb-mpesa' ),
                'type'        => 'number',
                'description' => __( 'The Paybill/Till Number to request payment to.', 'wc-kb-mpesa' ),
                'default'     => __( '', 'wc-kb-mpesa' ),
                'desc_tip'    => true,
            ],

			'till_number_desc'=>[
			    'title'=>__('','wc-kb-mpesa'),
                'type'=>'title',
                'description'=>__('If using till number, provide us with the <strong>Store Number</strong>','wc-kb-mpesa')
            ],


            'lnmo_store_number' => [
                'title'       => __( 'Store Number', 'wc-kb-mpesa' ),
                'type'        => 'number',
                'description' => __( 'Store Number is issued together with till number and office number', 'wc-kb-mpesa' ),
                'default'     => __( '', 'wc-kb-mpesa' ),
                'desc_tip'    => true,
            ],

			'lnmo_hr2' => [
				'title'       => __( ' ', 'wc-kb-mpesa' ),
				'type'        => 'title',
				'description' => sprintf( __( '<hr>', 'wc-kb-mpesa' )),
            ],

			'lnmo_title' => [
				'title'       => __( 'PLUGIN INFORMATION', 'wc-kb-mpesa' ),
				'type'        => 'title',
                'description' => 'This is what users will see on checkout'
            ],


            'lnmo_hr3' => [
                'title'       => __( '', 'wc-kb-mpesa' ),
                'type'        => 'title',
                'description' => sprintf( __( '<hr>', 'wc-kb-mpesa' )),
            ],


            'title' => [
                'title' => __( 'Title', 'wc-kb-mpesa' ),
                'type' => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'wc-kb-mpesa' ),
                'default' => __( 'Mpesa Payment', 'wc-kb-mpesa' ),
                'desc_tip'      => true,
            ],

			'description' => [
				'title'       => __( 'Description', 'wc-kb-mpesa' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your website.', 'wc-kb-mpesa' ),
				'default'     => __( 'Pay with MPESA.', 'wc-kb-mpesa' ),
				'desc_tip'    => true,
            ],
        ]);
    }
    
    public function process_payment( $order_id ) {
        global $woocommerce;
		$order = new WC_Order( $order_id );
        // Return thankyou redirect
        return array(
            'result' => 'success',
            'redirect' => $order->get_checkout_payment_url( true )
        );
	}

	/**
     * Checkout receipt page
     *
     * @return void
     */
    public function receipt_page( $order ) {
		$order = wc_get_order( $order );
		echo '<div class="wc-kb-mpesa-notices" id="wc-kb-mpesa-notices"></div>';
		echo '<p>'.__( 'Thank you for your order, please click the button below to pay with MPESA.', 'wc-kb-mpesa' ).'</p>';
		echo '<button class="button wc-kb-mpesa-pay-btn alt" id="wc-kb-mpesa-pay-btn" data-order-id="'.$order->get_id().'" data-order-thankyou-url="'.$order->get_checkout_order_received_url().'">
				<div class="wc-kb-mpesa-loader-wrap" id="wc-kb-mpesa-loader-wrap">
					<div class="wc-kb-mpesa-loader"></div>
					<span class="wc-kb-mpesa-loader-text">Waiting..</span>
				</div>
				<span class="wc-kb-mpesa-pay-text" id="wc-kb-mpesa-pay-text">Pay Now</span>
			</button> ';
		echo '<a class="button cancel" href="' . esc_url( $order->get_cancel_order_url() ) . '">';
		echo __( 'Cancel order &amp; restore cart', 'wc-kb-mpesa' ) . '</a>';
	}
	
	/**
	 * Logging method.
	 *
	 * @param string $message Log message.
	 * @param string $level   Optional. Default 'info'.
	 *     emergency|alert|critical|error|warning|notice|info|debug
	 */
	public static function log( $message, $level = 'info' ) {
		/*if ( self::$log_enabled ) {
			if ( empty( self::$log ) ) {
				self::$log = wc_get_logger();
			}
			self::$log->log( $level, $message, array( 'source' => 'wc_kb_mpesa' ) );
		}*/
	}
}
