<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/includes
 * @author     FutureVH <https://futurevh.com>
 */
class WC_Kb_Mpesa_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
