<?php

namespace Kabangi\Mpesa\LipaNaMpesaOnline;

use Kabangi\Mpesa\Engine\Core;

class STKPush{

    protected $endpoint = 'mpesa/stkpush/v1/processrequest';

    protected $engine;

    protected $validationRules = [
        'BusinessShortCode:BusinessShortCode' => 'required()({label} is required) | number',
        'Password:Password' => 'required()({label} is required)',
        'Timestamp:Timestamp' => 'required()({label} is required)',
        'TransactionType:TransactionType' => 'required()({label} is required)',
        'Amount:Amount' => 'required()({label} is required) | number()({label} should be a numeric value)',
        'PartyA:Party A' => 'required()({label} is required)',
        'PartyB:PartyB' => 'required()({label} is required)',
        'PhoneNumber:PhoneNumber' => 'required()({label} is required)',
        'CallBackURL:CallBackURL' => 'required()({label} is required) | website',
        'AccountReference:AccountReference' => 'required()({label} is required)',
        'TransactionDesc:TransactionDesc' => 'required()({label} is required)'
    ];

    /**
     * STK constructor.
     *
     * @param Core $engine
     */
    public function __construct(Core $engine)
    {
        $this->engine       = $engine;
        $this->engine->addValidationRules($this->validationRules);
    }


    /**
     * Initiate STK push request
     *
     * @param array $params
     *
     * @return array|bool|mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function submit($params = []){
        // Make sure all the indexes are in Uppercases as shown in docs
        $userParams = [];
        foreach ($params as $key => $value) {
            $userParams[ucwords($key)] = $value;
        }

        /**
         * Till Business type
         * BusinessShortCode & ShortCode uses StoreNumber
         * PartyB uses the till number
         */
        $time      = $this->engine->getCurrentRequestTime();
        $shortCode = $this->engine->config->get('mpesa.lnmo.short_code');
        $storeNumber=$this->engine->config->get('mpesa.lnmo.store_number');
        $passkey   = $this->engine->config->get('mpesa.lnmo.passkey');
        $partyB=$shortCode;
        $transactionType=$this->engine->config->get('mpesa.lnmo.default_transaction_type');
        if($this->engine->config->get('lnmo.merchant_type')=='till') {//if is till
            $partyB = $shortCode;// till number
            $shortCode=$storeNumber;// replace
            $transactionType= $this->engine->config->get('mpesa.lnmo.default_transaction_till_type');
        }

        $password  = \base64_encode($shortCode . $passkey . $time);

        // Computed and params from config file.
        $configParams = [
            'BusinessShortCode' => $shortCode,
            'CallBackURL'       => $this->engine->config->get('mpesa.lnmo.callback'),
            'TransactionType'   => $transactionType,
            'Password'          => $password,
            'PartyB'            => $partyB,
            'Timestamp'         => $time,
        ];

        // This gives precedence to params coming from user allowing them to override config params
        $body = array_merge($configParams,$userParams);
        if(empty($body['PartyA']) && !empty($body['PhoneNumber'])){
            $body['PartyA'] = $body['PhoneNumber'];
        }
        
        // Validate $body based on the daraja docs.
        $validationResponse = $this->engine->validateParams($body);
        if($validationResponse !== true){
            return $validationResponse;
        }

        ////till:5122745
        //$body['CallBackURL']='http://hostwithfuture.com/test/wc-api/WC_Kb_Mpesa_Gateway/';
        //$body['BusinessShortCode']='684099';
        //$body['PartyB']='801298';
        //$body['TransactionType']='CustomerBuyGoodsOnline';
        //$body['CommandID']="BusinessPayment";
        //echo json_encode($body);die;
        try {
            return $this->engine->makePostRequest([
                'endpoint' => $this->endpoint,
                'body' => $body
            ]);
        } catch (\Exception $exception) {
            return \json_decode($exception->getMessage());
        }
    }
}
