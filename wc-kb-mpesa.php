<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           WC_Kb_Mpesa
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerceKBMpesa
 * Plugin URI:        https://futurevh.com
 * Description:       This plugin allows you to add mpesa intergration to your woocommerce website.
 * Version:           2.1.6
 * Author:            FutureVH
 * Author URI:        https://futurevh.com
 * License:           GPL-2.0+
 * WC requires at least: 2.5
 * WC tested up to: 4.0.x
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       KBMpesa
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	return;
}

/**
 * Current plugin version.
 * Start at version 1.0.0 and uses SemVer - https://semver.org
 */
define( 'WC_KB_MPESA_VERSION', '1.0.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wc-kb-mpesa-activator.php
 */
function activate_wc_kb_mpesa() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wc-kb-mpesa-activator.php';
	WC_Kb_Mpesa_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wc-kb-mpesa-deactivator.php
 */
function deactivate_wc_kb_mpesa() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wc-kb-mpesa-deactivator.php';
	WC_Kb_Mpesa_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wc_kb_mpesa' );
register_deactivation_hook( __FILE__, 'deactivate_wc_kb_mpesa' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wc-kb-mpesa.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wc_kb_mpesa() {
	$plugin = new WC_Kb_Mpesa();
	$plugin->run();
	$plugin = plugin_basename(__FILE__); 
	add_filter("plugin_action_links_$plugin", 'plugin_settings_link' );
}


run_wc_kb_mpesa();

// Add settings link on plugin page
function plugin_settings_link($links) { 
	$url = get_admin_url() . 'admin.php?page=wc-settings&tab=checkout&section=wc_kb_mpesa';
	$settings_link = '<a href="'.$url.'">' . __( 'Settings', 'wc_kb_mpesa' ) . '</a>';
	array_unshift( $links, $settings_link );
	return $links;
}

