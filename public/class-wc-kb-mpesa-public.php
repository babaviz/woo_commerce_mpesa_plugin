<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Kb_Mpesa
 * @subpackage Kb_Mpesa/public
 * @author     FutureVH <https://futurevh.com>
 */
class WC_Kb_Mpesa_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $wc_kb_mpesa    The ID of this plugin.
	 */
	private $wc_kb_mpesa;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $wc_kb_mpesa       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $wc_kb_mpesa, $version ) {

		$this->wc_kb_mpesa = $wc_kb_mpesa;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in WC_Kb_Mpesa_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The WC_Kb_Mpesa_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->wc_kb_mpesa, plugin_dir_url( __FILE__ ) . 'css/wc-kb-mpesa-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in WC_Kb_Mpesa_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The WC_Kb_Mpesa_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->wc_kb_mpesa, plugin_dir_url( __FILE__ ) . 'js/wc-kb-mpesa-public.js', array( 'jquery' ), $this->version, false );
		
		$mpesa_nonce = wp_create_nonce( 'wc_kb_mpesa_token' );
		// Register STKPUSH ajax urls
		wp_localize_script( $this->wc_kb_mpesa, 'wc_kb_mpesa_ajax', [
				'stk_push' => [
					'ajax_url' => admin_url( 'admin-ajax.php' ),
					'nonce'    => $mpesa_nonce,
					'action'	  => 'wc_kb_mpesa_checkout'
				],
				'stk_push_status' => [
					'ajax_url' => admin_url( 'admin-ajax.php' ),
					'nonce'    => $mpesa_nonce,
					'action'	  => 'wc_kb_mpesa_checkout_status'
				],
				'order_checkout' => [
					'ajax_url' => admin_url( 'admin-ajax.php' ),
					'nonce'    => $mpesa_nonce,
					'action'	  => 'wc_kb_order_checkout_exists'
				],
				
			]
		);
	}

	public function add_mpesa_shortcode(){
		function mpesa_shortcode($atts = [], $content = null, $tag = ''){
			// normalize attribute keys, lowercase
			$atts = array_change_key_case((array)$atts, CASE_LOWER);
			// override default attributes with user attributes
			$mpesa_atts = shortcode_atts([
				'phone' => 'WordPress.org',
				'label' => 'Pay',
			], $atts, $tag);
			// start output
			$o = '';
			
			// start box
			$o .= '<input type="submit" id="mpesa-btn" class="submit mpesa-btn" value="'.esc_html__($mpesa_atts['label'], 'wc_kb_mpesa').'">';
			 
				// enclosing tags
				if (!is_null($content)) {
					// secure output by executing the_content filter hook on $content
					$o .= apply_filters('the_content', $content);
			 
					// run shortcode parser recursively
					$o .= do_shortcode($content);
				}
			 
				// return output
				return $o;
		}
		add_shortcode('mpesa', 'mpesa_shortcode');
	}

	public function payments_callback(){
		$is_our_callback = !empty($_GET['source']) && !empty($_GET['purpose']) 
		&& $_GET['source'] == 'mpesa' && $_GET['purpose'] == 'payment';

		if ( $_SERVER['REQUEST_METHOD'] === 'POST' && $is_our_callback == true) {
			$product = $_GET['product'];
			switch($product){
				case 'stkpush':
					$this->handle_stk_push_request();
					break;
				case 'stkquery':
					$this->handle_stk_query_request();
					break;
				case 'c2b':
					break;
				default:
					// Do nothing
			}
		}
	}

	private function handle_stk_push_request(){
		
	}

	private function handle_stk_query_request(){

	}

	private function handle_c2b_request(){

	}
}
